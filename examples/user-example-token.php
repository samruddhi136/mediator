<?php

// Start session
session_start();

if($_GET['token'] != "") {
	$_SESSION['access_token'] = $_GET['token'];
}
if(isset($_SESSION['access_token'])) {
	$url = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token='.$_SESSION['access_token'];
	$content = file_get_contents($url);
	$json = json_decode($content, true);	
	$email = $json['email'];
}

if($_GET['app'] != "") {
	$_SESSION['app'] = $_GET['app'];
}

if($_GET['timekey'] != "") {
	$_SESSION['timekey'] = $_GET['timekey'];
}

//
/*
if (isset($_GET['app']) AND isset($_GET['timekey'])) {

    $parsed_url = parse_url($_SERVER['HTTP_REFERER']);

    $app = $_GET['app'];
    $timekey = $_GET['timekey'];
    $_SESSION['app'] = $app;
    $_SESSION['host'] = $parsed_url['host'];
    $_SESSION['timekey'] = $timekey;
}

require_once realpath(dirname(__FILE__) . '/../src/Google/autoload.php'); */

// Include two files from google-php-client library in controller
//include_once APPPATH . "libraries/google-api-php-client-master/src/Google/Client.php";
//include_once APPPATH . "libraries/google-api-php-client-master/src/Google/Service/Oauth2.php";
// Store values in variables from project created in Google Developer Console
//$client_id = '359733337863-23vmgrs32b40kd60bcjk4sqpaqtkfvqm.apps.googleusercontent.com';
//$client_secret = 'kqh_KqnlOd1YNPdQxEu3HZnW';
//Email: sso@clariontechnologies.co.in
//Password: clarion123
/*
$client_id = '991089413582-j1plu22h4n2t2qo704h9edtqcdm5f4a0.apps.googleusercontent.com';
$client_secret = 'wvyWy_Xs4KzzlvM0WDhEI3yw';


$redirect_uri = 'http://mediator.clariontech.com/examples/user-example.php';

$simple_api_key = 'asdfasdf';

// Create Client Request to access Google API
$client = new Google_Client();
$client->setApplicationName("PHP Google OAuth Login Example");
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->setDeveloperKey($simple_api_key);
$client->addScope("https://www.googleapis.com/auth/userinfo.email");

// Send Client Request
$objOAuthService = new Google_Service_Oauth2($client);

// Add Access Token to Session
if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}

// Set Access Token to make Request
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    $client->setAccessToken($_SESSION['access_token']);
}

// Get User Data from Google and store them in $data
if ($client->getAccessToken()) {
    $userData = $objOAuthService->userinfo->get();
    $data['userData'] = $userData;
    $_SESSION['userData'] = $userData;
    $_SESSION['access_token'] = $client->getAccessToken();
} else {
    $authUrl = $client->createAuthUrl();
    $data['authUrl'] = $authUrl;
}

if (isset($authUrl)) {
    //   echo "<a class='login' href='" . $authUrl . "'>Connect Me!</a>";

    header('Location:' . $authUrl);
} else {
    if (is_array($_SESSION) && array_key_exists("application", $_SESSION)) {
//        print_r($_SESSION);
//        exit();
//		header('Location: http://' . $_SESSION["application"] . "?user=true");
    }
}*/

$encryption_key = $_SESSION['timekey'];
$encryptedemail = encrypt($email, $encryption_key);

// echo testing.

$query = array(
    'email' => $encryptedemail,
    'timekey' => $_SESSION['timekey'],
);

//$session_userdata = $_SESSION['userData'];
echo $session_app = $_SESSION['app'];
$session_host = $_SESSION['host'];

if ($session_host == 'himalaya') {
    $host = 'http://himalaya:81';
} else {
    $host = 'http://182.74.238.209:81';
}


//if (isset($session_userdata)) {
    if ($session_app == 'reminders') {
        header('Location: ' . $host . '/reminders/oauth2.php?' . http_build_query($query));
    }

    if ($session_app == 'clarion_projects') {
        header('Location: ' . $host . '/clarion_prj_authlogin/oauth2.php?' . http_build_query($query));
    }

    if ($session_app == 'spandan_conf') {
        header('Location: ' . $host . '/conf_Sign_in_Google/oauth2.php?' . http_build_query($query));
    }
    
    if ($session_app == 'helpdesk') {
        header('Location: ' . $host . '/helpdesk/oauth2.php?' . http_build_query($query));
    }
	
	 if ($session_app == 'mwikitech') {
        header('Location: ' . $host . '/mwiki/technology/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
	
    if ($session_app == 'mwikimanagers') {
        header('Location: ' . $host . '/mwiki/managers/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }

    if ($session_app == 'mwikidevelopers') {
        header('Location: ' . $host . '/mwiki/Developers/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }

    if ($session_app == 'mwikismd') {
        header('Location: ' . $host . '/mwiki/delivery_management/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }

    if ($session_app == 'mwikiadmin') {
        header('Location: ' . $host . '/mwiki/admin/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }

    if ($session_app == 'mwikicvp') {
        header('Location: ' . $host . '/mwiki/cvp/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }

    if ($session_app == 'mwikicvpbusinesses') {
        header('Location: ' . $host . '/mwiki/cvpbusinesses/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }

    if ($session_app == 'mwikidesign') {
        header('Location: ' . $host . '/mwiki/design/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }

    if ($session_app == 'mwikifinance') {
        header('Location: ' . $host . '/mwiki/finance/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikihr') {
        header('Location: ' . $host . '/mwiki/hr/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiiphone') {
        header('Location: ' . $host . '/mwiki/iphone/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiitadmin') {
        header('Location: ' . $host . '/mwiki/ITAdmin/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    
    if ($session_app == 'mwikimarketing') {
        header('Location: ' . $host . '/mwiki/marketing/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikinewspandan') {
		header("Location: http://himalaya/conf/login.php");
        //header('Location: ' . $host . '/mwiki/newspandan/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
       
    if ($session_app == 'mwikioperationsmanagement') {
        header('Location: ' . $host . '/mwiki/operations-management/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikippqa') {
        header('Location: ' . $host . '/mwiki/PPQA/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikipracticelead') {
        header('Location: ' . $host . '/mwiki/practicelead/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiQA') {
        header('Location: ' . $host . '/mwiki/QA/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
        
    if ($session_app == 'mwikiSA') {
        header('Location: ' . $host . '/mwiki/SA/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiSales') {
        header('Location: ' . $host . '/mwiki/Sales/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiSalesmanager') {
        header('Location: ' . $host . '/mwiki/Salesmanager/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiStrategyClarion') {
        header('Location: ' . $host . '/mwiki/Strategy@Clarion/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiTraining') {
        header('Location: ' . $host . '/mwiki/Training/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiTrainings') {
        header('Location: ' . $host . '/mwiki/Trainings/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiDCHead') {
        header('Location: ' . $host . '/mwiki/DC-Head/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikisalesmanagement') {
        header('Location: ' . $host . '/mwiki/salesmanagement/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiMobileSales') {
        header('Location: ' . $host . '/mwiki/Mobile-Sales/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'mwikiMobileApps') {
        header('Location: ' . $host . '/mwiki/Mobile-Sales/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'QMS') {
        header('Location: ' . $host . '/QMS/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'PEG') {
        header('Location: ' . $host . '/PEG/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'php') {
        header('Location: ' . $host . '/php/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    if ($session_app == 'dotnet') {
        header('Location: ' . $host . '/dotnet/index.php?title=Special:Authlogin&returnto=Main_Page&' . http_build_query($query));
    }
    
    if ($session_app == 'clarionAppraisals') {
       $auth_email = $_SESSION['userData']['email'];
       $encryption_key1 = $_SESSION['timekey'];
       $encryptedemail1 = rawurlencode( encrypt($auth_email , $encryption_key1));
       header('Location: http://himalaya:81/Clarion_Appraisals/oauth_val?email='.$encryptedemail1.'&timekey='.$encryption_key1);    
       exit;
    }
    
//}

function encrypt($pure_string, $encryption_key) {
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
    return $encrypted_string;
}

?>
